﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using XIVAchievements.Commands;
using XIVAchievements.Models;
using XIVAchievements.Views;

namespace XIVAchievements.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SettingsUpgrade();
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }
        #endregion

        #region PROPERTIES
        #region PANELS
        #region MAIN CONTENT


        private ObservableCollection<string> incompleteAchievements = new ObservableCollection<string>();
        public ObservableCollection<string> IncompleteAchievements
        {
            get
            {
                return incompleteAchievements;
            }
            set
            {
                incompleteAchievements = value;
                OnPropertyChanged("IncompleteAchievements");
            }
        }

        
        public int IncompleteCount
        {
            get
            {
                return IncompleteAchievements.Count();
            }
            set
            {
                OnPropertyChanged("IncompleteCount");
            }
        }



        private ObservableCollection<string> completeAchievements = new ObservableCollection<string>();
        public ObservableCollection<string> CompleteAchievements
        {
            get
            {
                return completeAchievements;
            }
            set
            {
                completeAchievements = value;
                OnPropertyChanged("CompleteAchievements");
            }
        }


        public int CompleteCount
        {
            get
            {
                return CompleteAchievements.Count();
            }
            set
            {
                OnPropertyChanged("CompleteCount");
            }
        }


        #endregion
        #endregion

        #region SETTINGS
        public string BGImage
        {
            get
            {
                OnPropertyChanged("BGOpacity");
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
                OnPropertyChanged("BGOpacity");
            }
        }
        
        public double BGOpacity
        {
            get
            {
                switch (Properties.Settings.Default.BackgroundImage)
                {
                    case 1:
                        return 0.1;
                    case 2:
                        return 0.1;
                    case 3:
                        return 0.3;
                    case 4:
                        return 0.6;
                    case 5:
                        return 0.2;
                    case 6:
                        return 0.6;
                    case 7:
                        return 0.2;
                    case 8:
                        return 0.2;
                    case 9:
                        return 0.1;
                    case 10:
                        return 0.5;
                    default:
                        return 0;
                }
            }
            set
            {
                OnPropertyChanged("BGOpacity");
            }
        }

        #endregion
        #region MISC
        private ProgramVersion programVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region MAIN COMMANDS
        private ICommand calc;
        public ICommand Calc
        {
            get
            {
                if (calc == null)
                {
                    calc = new RelayCommand(CalcEx, null);
                }
                return calc;
            }
        }
        private void CalcEx(object p)
        {
            int x = 0;
            try
            {
                x = Convert.ToInt32(p);
                if(p.ToString().Length < 7 || p.ToString().Length > 8)
                {
                    throw new Exception();
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Invalid Entry.\nPlease enter a 7 or 8 digit number only.");
                return;
            }

            System.Windows.Forms.MessageBox.Show("this may take a few moments...");
            Properties.Settings.Default.LastSearch = x.ToString();
            Properties.Settings.Default.Save();

            string Url = "http://eu.finalfantasyxiv.com/lodestone/character/" + x + "/achievement/kind/";

            try
            {
                HtmlWeb web = new HtmlWeb();
                web.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0";

                HtmlAgilityPack.HtmlDocument doc = web.Load(Url + "1");
                var BattleEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var BattleUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "2");
                var CharacterEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var CharacterUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "4");
                var ItemsEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var ItemsUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "5");
                var SynthesisEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var SynthesisUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "6");
                var GatheringEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var GatheringUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "8");
                var QuestsEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var QuestsUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "11");
                var ExplorationEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var ExplorationUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                doc = web.Load(Url + "12");
                var GCEarntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"already\"]/div/div/div/div/div[2]/div[1]/span[1]");
                var GCUnearntNodes = doc.DocumentNode.SelectNodes("html/body/div[4]/div[1]/div[2]/div/div/div[2]/ul/li/div[@class=\"yet\"]/div/div/div/div/div[2]/div[1]/span[1]");

                foreach (HtmlAgilityPack.HtmlNode NC in BattleEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in BattleUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in CharacterEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in CharacterUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in ItemsEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in ItemsUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in SynthesisEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in SynthesisUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in GatheringEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in GatheringUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in QuestsEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in QuestsUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in ExplorationEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in ExplorationUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in GCEarntNodes)
                {
                    CompleteAchievements.Add(NC.InnerText);
                }
                foreach (HtmlAgilityPack.HtmlNode NC in GCUnearntNodes)
                {
                    IncompleteAchievements.Add(NC.InnerText);
                }

                OnPropertyChanged("IncompleteCount");
                OnPropertyChanged("CompleteCount");
                System.Windows.Forms.MessageBox.Show("Complete!");
            }
            catch(Exception err)
            {
                System.Windows.Forms.MessageBox.Show(err.Message);
            }
        }

        private ICommand saveFile;
        public ICommand SaveFile
        {
            get
            {
                if (saveFile == null)
                {
                    saveFile = new RelayCommand(SaveFileEx, null);
                }
                return saveFile;
            }
        }
        private void SaveFileEx(object p)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            // set a default file name
            savefile.FileName = "XIVAchievements.txt";
            // set filters - this can be done in properties as well
            savefile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(savefile.FileName))
                {
                    sw.WriteLine("###################");
                    sw.WriteLine("# XIVAchievements #");
                    sw.WriteLine("###################");
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.WriteLine("## Complete Achievements ##");
                    sw.WriteLine();

                    foreach(string achievement in CompleteAchievements)
                    {
                        sw.WriteLine(achievement);
                    }

                    sw.WriteLine();
                    sw.WriteLine();
                    sw.WriteLine("## Incomplete Achievements ##");
                    sw.WriteLine();

                    foreach (string achievement in IncompleteAchievements)
                    {
                        sw.WriteLine(achievement);
                    }

                    sw.WriteLine();
                    sw.WriteLine();
                    sw.WriteLine("### EOF ###");
                }
            }
        }
        #endregion
        #region MISC
        private ICommand openPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (openPreferences == null)
                {
                    openPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return openPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
